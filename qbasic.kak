# builtin faces
face global Default white,blue
face global PrimarySelection blue,bright-white+fg
face global SecondarySelection blue,white+fg
face global PrimaryCursor bright-white,bright-blue+fg
face global SecondaryCursor white,bright-blue+fg
face global PrimaryCursorEol bright-white,cyan+fg
face global SecondaryCursorEol white,cyan+fg
face global LineNumbers default,default
face global LineNumberCursor default,default+r

face global MenuForeground white,black
face global MenuBackground black,white
face global MenuInfo cyan
face global Information black,yellow
face global Error black,red
face global DiagnosticWarning default,yellow
face global DiagnosticError default,red
face global StatusLine bright-white,cyan
face global StatusLineMode bright-yellow,default
face global StatusLineInfo black,default
face global StatusLineValue black,default
face global StatusCursor cyan,white
face global Prompt black,default

face global MatchingChar default,default+b
face global Whitespace bright-blue,default+f
face global WrapMarker bright-blue,default+f
face global BufferPadding bright-blue,default

# For Code
face global value bright-magenta
face global type default
face global variable default
face global module default
face global function default
face global string value
face global keyword default+b
face global operator keyword
face global attribute default
face global comment cyan
face global documentation comment
face global meta keyword
face global builtin keyword

# For markup
face global title default+bu
face global header default+b
face global mono green
face global block magenta
face global link bright-green+u
face global bullet cyan
face global list yellow

# kak-lsp faces
face global InlayDiagnosticError bright-red,default
face global InlayDiagnosticHint bright-green,default,
face global InlayDiagnosticInfo bright-cyan,default
face global InlayDiagnosticWarning bright-yellow,default
face global LineFlagError default,red
face global LineFlagHint default,green
face global LineFlagInfo default,cyan
face global LineFlagWarning default,yellow
