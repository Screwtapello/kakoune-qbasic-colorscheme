Kakoune QBasic Colorscheme
==========================

A colorscheme for the [Kakoune] editor
designed to resemble the appearance of Microsoft's [QBasic] editor
for MS-DOS.

Screenshot
----------

![A screenshot of Kakoune with the QBasic colorscheme](./qbasic.png)

This screenshot uses a [VGA] text font
and the standard [CGA] text mode palette,
just like a real MS-DOS PC running QBasic might use.

Installation
------------

Copy the included [`qbasic.kak`](./qbasic.kak) file
into `~/.config/kak/colors/`,
or just clone this entire repository into that directory.

[VGA]: https://int10h.org/oldschool-pc-fonts/fontlist/
[CGA]: https://en.wikipedia.org/wiki/List_of_8-bit_computer_hardware_graphics#CGA
[Kakoune]: https://kakoune.org/
[QBasic]: https://en.wikipedia.org/wiki/QBasic
